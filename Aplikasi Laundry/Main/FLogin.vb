﻿Imports System.Data.OleDb

Public Class FLogin
    Private Sub btnlogin_Click(sender As Object, e As EventArgs) Handles btnlogin.Click
        KoneksiDB()
        str = "select * from Users where username='" & txtusername.Text & "' and password='" & txtpassword.Text & "'"
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            Me.txtpassword.Text = ""



            Me.Hide()

            Dim loginUser As New CUser
            loginUser.SetDataById(dr.GetValue(dr.GetOrdinal("id_user")))

            Dim mainForm As New FMenu
            mainForm.dataUser = loginUser

            If loginUser.dataIdRole <> 1 Then
                mainForm.JenisCucianToolStripMenuItem.Enabled = False
                mainForm.UserToolStripMenuItem.Enabled = False
                mainForm.PegawaiToolStripMenuItem.Enabled = False
            End If

            mainForm.Show()
        Else
            MsgBox("Username atau Password Salah")
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        Me.Close()
    End Sub

    Private Sub txtpassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtpassword.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            btnlogin.PerformClick()
            e.Handled = True
        End If
    End Sub
End Class