﻿Public Class FMenu
    Public dataUser As CUser

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick
        dateTimeLabel.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
    End Sub

    Private Sub FMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dateTimeLabel.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
        Timer.Start()
    End Sub

    Private Sub exitBtn_Click(sender As Object, e As EventArgs) Handles exitBtn.Click
        Application.Exit()
    End Sub

    Private Sub logoutBtn_Click(sender As Object, e As EventArgs) Handles logoutBtn.Click
        Me.Hide()
        Me.dataUser = New CUser
        FLogin.Show()
    End Sub

    Private Sub cucianBtn_Click(sender As Object, e As EventArgs) Handles cucianBtn.Click
        Dim cucianWindow As New FCucian With {
            .dataUser = Me.dataUser
        }
        cucianWindow.ShowDialog()
    End Sub

    Private Sub FMenu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Application.Exit()
    End Sub

    Private Sub ambilCucianBtn_Click(sender As Object, e As EventArgs) Handles ambilCucianBtn.Click
        Dim cucianWindow As New FAmbilCucian With {
            .dataUser = Me.dataUser
        }
        cucianWindow.ShowDialog()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogoutToolStripMenuItem.Click
        Me.Hide()
        Me.dataUser = New CUser
        FLogin.Show()
    End Sub

    Private Sub JenisCucianToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JenisCucianToolStripMenuItem.Click
        Dim window As New FJenisCucian
        window.ShowDialog()
    End Sub

    Private Sub PelangganToolStripMenuItem_Click(sender As Object, e As EventArgs)
        'Dim window As New FPelanggan
        'window.ShowDialog()
    End Sub

    Private Sub UserToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UserToolStripMenuItem.Click
        Dim window As New FUser
        window.ShowDialog()
    End Sub

    Private Sub PegawaiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegawaiToolStripMenuItem.Click
        Dim window As New FPegawai
        window.ShowDialog()
    End Sub
End Class
