﻿Imports System.Data.OleDb

Public Class CPelanggan
    Private IdPelanggan As Integer
    Public Property VIdPelanggan() As Integer
        Get
            Return IdPelanggan
        End Get
        Set(ByVal value As Integer)
            IdPelanggan = value
        End Set
    End Property

    Private NamaPelanggan As String
    Public Property VNamaPelanggan() As String
        Get
            Return NamaPelanggan
        End Get
        Set(ByVal value As String)
            NamaPelanggan = value
        End Set
    End Property

    Private AlamatPelanggan As String
    Public Property VAlamatPelanggan() As String
        Get
            Return AlamatPelanggan
        End Get
        Set(ByVal value As String)
            AlamatPelanggan = value
        End Set
    End Property

    Private NoTelpPelanggan As String
    Public Property VNoTelpPelanggan() As String
        Get
            Return NoTelpPelanggan
        End Get
        Set(ByVal value As String)
            NoTelpPelanggan = value
        End Set
    End Property

    Public Function GetPelanggan(ByVal id_pelanggan As Integer) As Boolean
        KoneksiDB()

        str = "select * from Pelanggan where id_pelanggan = " & id_pelanggan

        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader

        If dr.HasRows Then
            dr.Read()
            Me.IdPelanggan = dr.GetValue(dr.GetOrdinal("id_pelanggan"))
            Me.NamaPelanggan = dr.GetValue(dr.GetOrdinal("nama_pelanggan"))
            Me.AlamatPelanggan = dr.GetValue(dr.GetOrdinal("alamat_pelanggan"))
            Me.NoTelpPelanggan = dr.GetValue(dr.GetOrdinal("no_telp_pelanggan"))

            Return True
        Else
            Return False
        End If

        dr.Close()

        KoneksiDB(True)
    End Function

    Public Function GetPelangganByName(ByVal nama_pelanggan As String) As Boolean
        KoneksiDB()

        str = "select * from Pelanggan where nama_pelanggan = '" & nama_pelanggan & "'"

        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader

        If dr.HasRows Then
            dr.Read()
            Me.IdPelanggan = dr.GetValue(dr.GetOrdinal("id_pelanggan"))
            Me.NamaPelanggan = dr.GetValue(dr.GetOrdinal("nama_pelanggan"))
            Me.AlamatPelanggan = dr.GetValue(dr.GetOrdinal("alamat_pelanggan"))
            Me.NoTelpPelanggan = dr.GetValue(dr.GetOrdinal("no_telp_pelanggan"))

            Return True
        Else
            Return False
        End If

        dr.Close()

        KoneksiDB(True)
    End Function
End Class
