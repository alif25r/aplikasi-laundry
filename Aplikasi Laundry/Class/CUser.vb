﻿Imports System.Data.OleDb

Public Class CUser
    'Setter and Getter
    Public Property dataId() As Integer
    Public Property dataUsername() As String
    Public Property dataPassword() As String
    Public Property dataIdRole() As Integer
    Public Property dataIdPegawai() As Integer
    Public Property dataStatus() As Integer
    Public Property dataNama() As String
    Public Property dataAlamat() As String
    Public Property dataTelp() As String
    Public Property dataIdJabatan() As Integer
    Public Property dataJabatan() As String
    'End Setter And Getter

    Public Sub SetDataById(ByVal id_user As Integer)
        KoneksiDB()

        Me.dataId = id_user

        'Get User
        str = "select * from Users where id_user = " & id_user
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        dr.Read()
        Me.dataIdPegawai = dr.GetValue(dr.GetOrdinal("id_pegawai"))
        Me.dataIdRole = dr.GetValue(dr.GetOrdinal("id_role"))
        Me.dataUsername = dr.GetValue(dr.GetOrdinal("username"))
        Me.dataStatus = dr.GetValue(dr.GetOrdinal("status"))

        'Get Pegawai
        str = "select * from Pegawai where id_pegawai = " & Me.dataIdPegawai
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        dr.Read()
        Me.dataNama = dr.GetValue(dr.GetOrdinal("nama_pegawai"))
        Me.dataAlamat = dr.GetValue(dr.GetOrdinal("alamat_pegawai"))
        Me.dataTelp = dr.GetValue(dr.GetOrdinal("no_hp_pegawai"))
        Me.dataIdJabatan = dr.GetValue(dr.GetOrdinal("id_jabatan"))

        'Get Jabatan
        str = "select * from Jabatan where id_jabatan = " & Me.dataIdJabatan
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        dr.Read()
        Me.dataJabatan = dr.GetValue(dr.GetOrdinal("jabatan"))

        KoneksiDB(True)
    End Sub

    Public Sub Load(ByVal dgv As DataGridView)
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable

        Me.AddHeader(dt)

        KoneksiDB()

        str = "SELECT Users.id_user AS ID, Pegawai.nama_pegawai AS [Nama Pegawai], Users.username AS [Username], 
            Roles.nama_role AS [Role], IIF(Users.status = 1, 'Aktif', 'Tidak Aktif') AS [Status]
            FROM Roles INNER JOIN (Pegawai INNER JOIN Users ON Pegawai.id_pegawai = Users.id_pegawai) ON Roles.id_role = Users.id_role"

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt
        dgv.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        KoneksiDB(True)
    End Sub

    Public Sub Filter(ByVal dgv As DataGridView, ByVal params As List(Of String))
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable
        Dim where As String = "Where 1 = 1 "

        Me.AddHeader(dt)

        If params.Count > 0 Then
            where += " and " + String.Join(" and ", params.ToArray)
        End If

        KoneksiDB()

        str = "SELECT Users.id_user AS ID, Pegawai.nama_pegawai AS [Nama Pegawai], Users.username AS [Username], 
            Roles.nama_role AS [Role], IIF(Users.status = 1, 'Aktif', 'Tidak Aktif') AS [Status]
            FROM Roles INNER JOIN (Pegawai INNER JOIN Users ON Pegawai.id_pegawai = Users.id_pegawai) ON Roles.id_role = Users.id_role"

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt
        dgv.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        KoneksiDB(True)
    End Sub

    Private Sub AddHeader(ByRef dt As DataTable)
        dt.Columns.Add("ID")
        dt.Columns.Add("Nama Pegawai")
        dt.Columns.Add("Username")
        dt.Columns.Add("Role")
        dt.Columns.Add("Status")
        dt.PrimaryKey = {dt.Columns.Item(0)}
    End Sub

    Public Sub LoadPegawaiNonUser(ByVal cbx As ComboBox, Optional ByVal id_pegawai As Integer = 0)
        KoneksiDB()

        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable

        If id_pegawai = 0 Then
            str = "SELECT id_pegawai, nama_pegawai FROM Pegawai WHERE id_pegawai not in (SELECT id_pegawai FROM Users)"
        Else
            str = "SELECT id_pegawai, nama_pegawai FROM Pegawai WHERE id_pegawai = " & id_pegawai
        End If
        adapter = New OleDbDataAdapter(str, conn)
            adapter.Fill(dt)

        cbx.DataSource = dt
        cbx.DisplayMember = "nama_pegawai"
        cbx.ValueMember = "id_pegawai"

        KoneksiDB(True)
    End Sub

    Public Function Add() As Boolean
        KoneksiDB()

        str = "Insert into Users([username], [password], [id_role], [id_pegawai], [status]) " &
            "Values ('" & Me.dataUsername & "', '" & Me.dataPassword & "', " & Me.dataIdRole & ", " & Me.dataIdPegawai & ", " & Me.dataStatus & ")"
        cmd = New OleDbCommand(str, conn)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function

    Public Function Update() As Boolean
        KoneksiDB()

        If Me.dataPassword Is Nothing Then
            str = "Update Users Set username = ?, status = ?, id_role = ? Where id_user = ?"
            cmd = New OleDbCommand(str, conn)

            cmd.Parameters.AddWithValue("username", Me.dataUsername)
            cmd.Parameters.AddWithValue("status", Me.dataStatus)
            cmd.Parameters.AddWithValue("id_role", Me.dataIdRole)
            cmd.Parameters.AddWithValue("id_user", Me.dataId)

        Else
            str = "Update Users Set [username] = ?, [status] = ?, [id_role] = ?, [password] = ? Where [id_user] = ?"
            cmd = New OleDbCommand(str, conn)

            cmd.Parameters.AddWithValue("username", Me.dataUsername)
            cmd.Parameters.AddWithValue("status", Me.dataStatus)
            cmd.Parameters.AddWithValue("id_role", Me.dataIdRole)
            cmd.Parameters.AddWithValue("password", Me.dataPassword)
            cmd.Parameters.AddWithValue("id_user", Me.dataId)
        End If

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function
End Class
