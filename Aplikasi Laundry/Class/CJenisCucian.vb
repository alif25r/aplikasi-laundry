﻿Imports System.Data.OleDb

Public Class CJenisCucian
    Public Property IdJenisCucian() As Integer
    Public Property JenisCucian() As String
    Public Property Harga() As Integer
    Public Property Satuan() As String

    Public Sub New(Optional ByVal id_jenis_cucian As Integer = Nothing)
        If Not IsNothing(id_jenis_cucian) Then
            KoneksiDB()

            str = "select * from Jenis_Cucian where id_jenis_cucian = " & id_jenis_cucian

            cmd = New OleDbCommand(str, conn)
            dr = cmd.ExecuteReader

            If dr.HasRows Then
                dr.Read()
                Me.IdJenisCucian = dr.GetValue(dr.GetOrdinal("id_jenis_cucian"))
                Me.JenisCucian = dr.GetValue(dr.GetOrdinal("jenis_cucian"))
                Me.Harga = dr.GetValue(dr.GetOrdinal("harga"))
                Me.Satuan = dr.GetValue(dr.GetOrdinal("satuan"))
            End If

            dr.Close()

            KoneksiDB(True)
        End If
    End Sub

    Public Sub Load(ByVal dgv As DataGridView)
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable

        Me.addHeader(dt)

        KoneksiDB()

        str = "SELECT id_jenis_cucian AS ID, jenis_cucian AS [Jenis Cucian], harga AS [Harga], satuan AS [Satuan] FROM Jenis_Cucian"

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt

        KoneksiDB(True)
    End Sub

    Private Sub addHeader(ByRef dt As DataTable)
        dt.Columns.Add("ID")
        dt.Columns.Add("Jenis Cucian")
        dt.Columns.Add("Harga")
        dt.PrimaryKey = {dt.Columns.Item(0)}
    End Sub

    Public Sub SetDataById(ByVal id_jenis_cucian As Integer)
        KoneksiDB()

        Me.IdJenisCucian = id_jenis_cucian

        'Get Pegawai
        str = "SELECT * FROM Jenis_Cucian
            WHERE id_jenis_cucian = " & id_jenis_cucian
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        dr.Read()

        Me.JenisCucian = dr.GetValue(dr.GetOrdinal("jenis_cucian"))
        Me.Harga = dr.GetValue(dr.GetOrdinal("harga"))
        Me.Satuan = dr.GetValue(dr.GetOrdinal("satuan"))

        KoneksiDB(True)
    End Sub

    Public Function Add() As Boolean
        KoneksiDB()

        str = "Insert into Jenis_Cucian([jenis_cucian],[harga],[satuan]) " &
            "Values ('" & Me.JenisCucian & "', '" & Me.Harga & "', '" & Me.Satuan & "')"
        cmd = New OleDbCommand(str, conn)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function

    Public Function Edit() As Boolean
        KoneksiDB()

        str = "Update Jenis_Cucian Set jenis_cucian = ?, harga = ?, satuan= ? Where id_jenis_cucian = ?"
        cmd = New OleDbCommand(str, conn)

        cmd.Parameters.AddWithValue("jenis_cucian", Me.JenisCucian)
        cmd.Parameters.AddWithValue("harga", Me.Harga)
        cmd.Parameters.AddWithValue("satuan", Me.Satuan)
        cmd.Parameters.AddWithValue("id_jenis_satuan", Me.IdJenisCucian)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function

End Class
