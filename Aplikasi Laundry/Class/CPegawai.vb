﻿Imports System.Data.OleDb

Public Class CPegawai
    Private id_pegawai As Integer
    Public Property dataId() As Integer
        Get
            Return id_pegawai
        End Get
        Set(ByVal value As Integer)
            id_pegawai = value
        End Set
    End Property

    Private nama_pegawai As String
    Public Property dataNama() As String
        Get
            Return nama_pegawai
        End Get
        Set(ByVal value As String)
            nama_pegawai = value
        End Set
    End Property

    Private alamat_pegawai As String
    Public Property dataAlamat() As String
        Get
            Return alamat_pegawai
        End Get
        Set(ByVal value As String)
            alamat_pegawai = value
        End Set
    End Property

    Private telp_pegawai As String
    Public Property dataTelp() As String
        Get
            Return telp_pegawai
        End Get
        Set(ByVal value As String)
            telp_pegawai = value
        End Set
    End Property

    Private id_jabatan As Integer
    Public Property dataIdJabatan() As Integer
        Get
            Return id_jabatan
        End Get
        Set(ByVal value As Integer)
            id_jabatan = value
        End Set
    End Property

    Private nama_jabatan As String
    Public Property dataJabatan() As String
        Get
            Return nama_jabatan
        End Get
        Set(ByVal value As String)
            nama_jabatan = value
        End Set
    End Property

    Private status As Integer
    Public Property dataStatus() As Integer
        Get
            Return status
        End Get
        Set(ByVal value As Integer)
            status = value
        End Set
    End Property

    Public Sub Load(ByVal dgv As DataGridView)
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable

        Me.addHeader(dt)

        KoneksiDB()

        str = "SELECT Pegawai.id_pegawai AS ID, Pegawai.nama_pegawai AS [Nama Pegawai], Pegawai.alamat_pegawai AS [Alamat], 
            Pegawai.no_hp_pegawai AS [No Telp], Jabatan.jabatan AS Jabatan, IIF(status = 1, 'Aktif', 'Tidak Aktif') AS [Status]
            FROM Pegawai 
            INNER JOIN Jabatan ON Pegawai.id_jabatan = Jabatan.id_jabatan"

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt

        KoneksiDB(True)
    End Sub

    Public Sub Filter(ByVal dgv As DataGridView, ByVal params As List(Of String))
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable
        Dim where As String = "Where 1 = 1 "

        Me.addHeader(dt)

        If params.Count > 0 Then
            where += " and " + String.Join(" and ", params.ToArray)
        End If

        KoneksiDB()

        str = "SELECT Pegawai.id_pegawai AS ID, Pegawai.nama_pegawai AS [Nama Pegawai], Pegawai.alamat_pegawai AS [Alamat], 
            Pegawai.no_hp_pegawai AS [No Telp], Jabatan.jabatan AS Jabatan, status AS [Status]
            FROM Pegawai 
            INNER JOIN Jabatan ON Pegawai.id_jabatan = Jabatan.id_jabatan " + where

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt

        KoneksiDB(True)
    End Sub

    Private Sub addHeader(ByRef dt As DataTable)
        dt.Columns.Add("ID")
        dt.Columns.Add("Nama Pegawai")
        dt.Columns.Add("Alamat")
        dt.Columns.Add("No Telp")
        dt.Columns.Add("Jabatan")
        dt.Columns.Add("Status")
        dt.PrimaryKey = {dt.Columns.Item(0)}
    End Sub

    Public Sub SetDataById(ByVal id_pegawai As Integer)
        KoneksiDB()

        Me.id_pegawai = id_pegawai

        'Get Pegawai
        str = "SELECT * FROM Pegawai 
            INNER JOIN Jabatan ON Pegawai.id_jabatan = Jabatan.id_jabatan
            WHERE id_pegawai = " & id_pegawai
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader
        dr.Read()

        Me.dataId = dr.GetValue(dr.GetOrdinal("id_pegawai"))
        Me.dataNama = dr.GetValue(dr.GetOrdinal("nama_pegawai"))
        Me.dataAlamat = dr.GetValue(dr.GetOrdinal("alamat_pegawai"))
        Me.dataTelp = dr.GetValue(dr.GetOrdinal("no_hp_pegawai"))
        Me.dataStatus = dr.GetValue(dr.GetOrdinal("status"))
        Me.dataIdJabatan = dr.GetValue(dr.GetOrdinal("Jabatan.id_jabatan"))
        Me.dataJabatan = dr.GetValue(dr.GetOrdinal("jabatan"))

        KoneksiDB(True)
    End Sub

    Public Function Add() As Boolean
        KoneksiDB()

        str = "Insert into Pegawai([nama_pegawai],[alamat_pegawai],[no_hp_pegawai],[id_jabatan],[status]) " &
            "Values ('" & Me.dataNama & "', '" & Me.dataAlamat & "', '" & Me.dataTelp & "', " & Me.dataIdJabatan & ", " & Me.dataStatus & ")"
        cmd = New OleDbCommand(str, conn)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function

    Public Function Edit() As Boolean
        KoneksiDB()

        str = "Update Pegawai Set nama_pegawai = ?, alamat_pegawai = ?, no_hp_pegawai = ?, id_jabatan = ?, status = ? Where id_pegawai = ?"
        cmd = New OleDbCommand(str, conn)

        cmd.Parameters.AddWithValue("nama_pegawai", Me.dataNama)
        cmd.Parameters.AddWithValue("alamat_pegawai", Me.dataAlamat)
        cmd.Parameters.AddWithValue("no_hp_pegawai", Me.dataTelp)
        cmd.Parameters.AddWithValue("id_jabatan", Me.dataIdJabatan)
        cmd.Parameters.AddWithValue("id_jabatan", Me.dataStatus)
        cmd.Parameters.AddWithValue("id_pegawai", Me.dataId)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)

            Return False
        End Try

        KoneksiDB(True)
    End Function
End Class
