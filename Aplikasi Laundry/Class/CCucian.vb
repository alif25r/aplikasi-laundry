﻿Imports System.Data.OleDb

Public Class CCucian
    Public Property IdCucian() As Integer

    Public Property NoFaktur() As String

    Public Property IdPegawai() As Integer

    Public Property IdPelanggan() As Integer

    Public Property IdJenisCucian() As Integer

    Public Property Jumlah() As Decimal

    Public Property Satuan() As String

    Public Property Harga() As Integer

    Public Property TotalHarga() As Decimal

    Public Property TglMasuk() As Date

    Public Property TglSelesai() As Date

    Public Property TglKeluar() As Date

    Public Function SimpanCucian() As Boolean
        Dim dataJenisCucian As New CJenisCucian(Me.IdJenisCucian)
        Me.Harga = dataJenisCucian.Harga
        Me.Satuan = dataJenisCucian.Satuan
        Me.CreateFaktur()

        KoneksiDB()

        str = "Insert into Cucian([no_faktur],[id_pegawai],[id_pelanggan],[id_jenis_cucian],[jumlah]," &
            "[satuan],[harga],[total_harga],[tgl_masuk]) Values ('" & Me.NoFaktur & "', " & Me.IdPegawai & ", " & Me.IdPelanggan &
            ", " & Me.IdJenisCucian & ", " & Me.Jumlah & ", '" & Me.Satuan & "', " & Me.Harga & ", " & Me.TotalHarga & ", '" & Me.TglMasuk.ToString & "')"
        cmd = New OleDbCommand(str, conn)

        Try
            cmd.ExecuteNonQuery()
            cmd.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        KoneksiDB(True)

        Return True
    End Function

    Private Sub CreateFaktur()
        Me.NoFaktur = Me.TglMasuk.Ticks
    End Sub

    Public Sub LoadCucian(ByVal dgv As DataGridView, Optional ByVal ambil As Boolean = False)
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable

        Me.addHeaderCucian(dt)

        KoneksiDB()

        str = "Select id_cucian as [ID], no_faktur as [No Faktur], nama_pelanggan as [Nama Pelanggan], nama_pegawai as [Nama Pegawai], jenis_cucian as [Jenis Cucian], 
            jumlah, harga, total_harga as [Total], tgl_masuk as [Tgl Masuk], tgl_selesai as [Tgl Selesai], tgl_keluar as [Tgl Keluar]
            From View_Cucian"

        If ambil Then
            str += " where tgl_selesai is not null"
        End If

        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt
        dgv.Columns("Harga").DefaultCellStyle.Format = "N2"
        dgv.Columns("Harga").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("Total").DefaultCellStyle.Format = "N2"
        dgv.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        KoneksiDB(True)
    End Sub

    Public Sub FilterCucian(ByVal dgv As DataGridView, ByVal params As List(Of String), Optional ByVal ambil As Boolean = False)
        Dim adapter As OleDbDataAdapter
        Dim dt As New DataTable
        Dim where As String = "Where 1 = 1 "

        Me.addHeaderCucian(dt)

        If params.Count > 0 Then
            where += " and " + String.Join(" and ", params.ToArray)
        End If

        If ambil Then
            str += " and tgl_selesai is not null"
        End If

        KoneksiDB()

        str = "Select id_cucian as [ID], no_faktur as [No Faktur], nama_pelanggan as [Nama Pelanggan], nama_pegawai as [Nama Pegawai], jenis_cucian as [Jenis Cucian], 
            jumlah, harga, total_harga as [Total], tgl_masuk as [Tgl Masuk], tgl_selesai as [Tgl Selesai], tgl_keluar as [Tgl Keluar]
            From View_Cucian " + where
        adapter = New OleDbDataAdapter(str, conn)
        adapter.Fill(dt)

        dgv.DataSource = dt
        dgv.Columns("Harga").DefaultCellStyle.Format = "N2"
        dgv.Columns("Harga").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("Total").DefaultCellStyle.Format = "N2"
        dgv.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        KoneksiDB(True)
    End Sub

    Private Sub addHeaderCucian(ByRef dt As DataTable)
        dt.Columns.Add("ID")
        dt.Columns.Add("No Faktur")
        dt.Columns.Add("Nama Pelanggan")
        dt.Columns.Add("Nama Pegawai")
        dt.Columns.Add("Jenis Cucian")
        dt.Columns.Add("Jumlah")
        dt.Columns.Add("Harga", GetType(Decimal))
        dt.Columns.Add("Total", GetType(Decimal))
        dt.Columns.Add("Tgl Masuk")
        dt.Columns.Add("Tgl Selesai")
        dt.Columns.Add("Tgl Keluar")

        dt.PrimaryKey = {dt.Columns.Item(0)}
    End Sub

    Public Sub cucianSelesai(ByVal idCucian As Integer)
        KoneksiDB()

        str = "Update Cucian Set tgl_selesai = NOW() Where id_cucian = ?"
        cmd = New OleDbCommand(str, conn)

        cmd.Parameters.AddWithValue("id_cucian", Int32.Parse(idCucian.ToString))

        cmd.ExecuteNonQuery()

        KoneksiDB(True)
    End Sub

    Public Sub cucianKeluar(ByVal idCucian As Integer)
        KoneksiDB()

        str = "Update Cucian Set tgl_keluar = NOW() Where id_cucian = ?"
        cmd = New OleDbCommand(str, conn)

        cmd.Parameters.AddWithValue("id_cucian", Int32.Parse(idCucian.ToString))

        cmd.ExecuteNonQuery()

        KoneksiDB(True)
    End Sub
End Class
