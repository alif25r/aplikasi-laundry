﻿Public Class FJenisCucian
    Private CJenisCucian As New CJenisCucian

    Private Sub FJenisCucian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CJenisCucian.Load(dgvJenisCucian)
    End Sub

    Private Sub dgvJenisCucian_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJenisCucian.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgvJenisCucian.Rows(e.RowIndex)

            'Cell Value - Data JenisCucian
            Dim id = selectedRow.Cells(0).Value
            Me.CJenisCucian.SetDataById(id)

            Me.tbxJenisCucian.Text = Me.CJenisCucian.JenisCucian
            Me.tbxHarga.Text = Me.CJenisCucian.Harga
            Me.tbxSatuan.Text = Me.CJenisCucian.Satuan

            btnTambah.Enabled = False
            btnUbah.Enabled = True
        End If
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Me.tbxJenisCucian.Text = ""
        Me.tbxHarga.Text = ""
        Me.tbxSatuan.Text = ""

        Me.CJenisCucian = New CJenisCucian

        btnTambah.Enabled = True
        btnUbah.Enabled = False
    End Sub

    Private Sub btnUbah_Click(sender As Object, e As EventArgs) Handles btnUbah.Click
        Me.CJenisCucian.JenisCucian = tbxJenisCucian.Text
        Me.CJenisCucian.Harga = tbxHarga.Text
        Me.CJenisCucian.Satuan = tbxSatuan.Text

        If (Me.CJenisCucian.JenisCucian <> "" And Me.CJenisCucian.Harga.ToString <> "" And Me.CJenisCucian.Satuan <> "") Then
            Dim resultAdd = Me.CJenisCucian.Edit()

            If resultAdd Then
                MessageBox.Show("Data Jenis Cucian Berhasil Diubah", "Form Response")
                Me.CJenisCucian.Load(dgvJenisCucian)
                btnReset.PerformClick()
            End If
        Else
            MessageBox.Show("Silakan lengkapi form Jenis Cucian", "Form Validation")
        End If
    End Sub

    Private Sub btnTambah_Click(sender As Object, e As EventArgs) Handles btnTambah.Click
        Me.CJenisCucian.JenisCucian = tbxJenisCucian.Text
        Me.CJenisCucian.Harga = tbxHarga.Text
        Me.CJenisCucian.Satuan = tbxSatuan.Text

        If (Me.CJenisCucian.JenisCucian <> "" And Me.CJenisCucian.Harga.ToString <> "" And Me.CJenisCucian.Satuan <> "") Then
            Dim resultAdd = Me.CJenisCucian.Add()

            If resultAdd Then
                MessageBox.Show("Data Jenis Cucian Berhasil Ditambahkan", "Form Response")
                Me.CJenisCucian.Load(dgvJenisCucian)
                btnReset.PerformClick()
            End If
        Else
            MessageBox.Show("Silakan lengkapi form Jenis Cucian", "Form Validation")
        End If
    End Sub
End Class