﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FPegawai
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FPegawai))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvPegawai = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxStatus = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnUbah = New System.Windows.Forms.Button()
        Me.btnTambah = New System.Windows.Forms.Button()
        Me.cbxJabatan = New System.Windows.Forms.ComboBox()
        Me.JabatanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Database_LaundryDataSet = New WindowsApplication1.Database_LaundryDataSet()
        Me.tbxHP = New System.Windows.Forms.TextBox()
        Me.tbxAlamat = New System.Windows.Forms.RichTextBox()
        Me.tbxNamaPegawai = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.JabatanTableAdapter = New WindowsApplication1.Database_LaundryDataSetTableAdapters.JabatanTableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPegawai, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.JabatanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Database_LaundryDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.dgvPegawai)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(218, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(577, 412)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "List Pegawai"
        '
        'dgvPegawai
        '
        Me.dgvPegawai.AllowUserToAddRows = False
        Me.dgvPegawai.AllowUserToDeleteRows = False
        Me.dgvPegawai.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPegawai.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPegawai.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPegawai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPegawai.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPegawai.Location = New System.Drawing.Point(3, 16)
        Me.dgvPegawai.MultiSelect = False
        Me.dgvPegawai.Name = "dgvPegawai"
        Me.dgvPegawai.ReadOnly = True
        Me.dgvPegawai.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPegawai.Size = New System.Drawing.Size(571, 393)
        Me.dgvPegawai.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.cbxStatus)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btnReset)
        Me.GroupBox2.Controls.Add(Me.btnUbah)
        Me.GroupBox2.Controls.Add(Me.btnTambah)
        Me.GroupBox2.Controls.Add(Me.cbxJabatan)
        Me.GroupBox2.Controls.Add(Me.tbxHP)
        Me.GroupBox2.Controls.Add(Me.tbxAlamat)
        Me.GroupBox2.Controls.Add(Me.tbxNamaPegawai)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 412)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Form Pegawai"
        '
        'cbxStatus
        '
        Me.cbxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.FormattingEnabled = True
        Me.cbxStatus.Items.AddRange(New Object() {"Tidak Aktif", "Aktif"})
        Me.cbxStatus.Location = New System.Drawing.Point(9, 275)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Size = New System.Drawing.Size(185, 21)
        Me.cbxStatus.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 258)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Status"
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.Location = New System.Drawing.Point(9, 375)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(185, 29)
        Me.btnReset.TabIndex = 11
        Me.btnReset.Text = "RESET"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnUbah
        '
        Me.btnUbah.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnUbah.Enabled = False
        Me.btnUbah.Location = New System.Drawing.Point(9, 340)
        Me.btnUbah.Name = "btnUbah"
        Me.btnUbah.Size = New System.Drawing.Size(185, 29)
        Me.btnUbah.TabIndex = 9
        Me.btnUbah.Text = "UBAH"
        Me.btnUbah.UseVisualStyleBackColor = False
        '
        'btnTambah
        '
        Me.btnTambah.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnTambah.Location = New System.Drawing.Point(9, 305)
        Me.btnTambah.Name = "btnTambah"
        Me.btnTambah.Size = New System.Drawing.Size(185, 29)
        Me.btnTambah.TabIndex = 8
        Me.btnTambah.Text = "TAMBAH"
        Me.btnTambah.UseVisualStyleBackColor = False
        '
        'cbxJabatan
        '
        Me.cbxJabatan.DataSource = Me.JabatanBindingSource
        Me.cbxJabatan.DisplayMember = "jabatan"
        Me.cbxJabatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxJabatan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxJabatan.FormattingEnabled = True
        Me.cbxJabatan.Location = New System.Drawing.Point(9, 228)
        Me.cbxJabatan.Name = "cbxJabatan"
        Me.cbxJabatan.Size = New System.Drawing.Size(185, 21)
        Me.cbxJabatan.TabIndex = 7
        Me.cbxJabatan.ValueMember = "id_jabatan"
        '
        'JabatanBindingSource
        '
        Me.JabatanBindingSource.DataMember = "Jabatan"
        Me.JabatanBindingSource.DataSource = Me.Database_LaundryDataSet
        '
        'Database_LaundryDataSet
        '
        Me.Database_LaundryDataSet.DataSetName = "Database_LaundryDataSet"
        Me.Database_LaundryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'tbxHP
        '
        Me.tbxHP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxHP.Location = New System.Drawing.Point(9, 180)
        Me.tbxHP.Name = "tbxHP"
        Me.tbxHP.Size = New System.Drawing.Size(185, 20)
        Me.tbxHP.TabIndex = 6
        '
        'tbxAlamat
        '
        Me.tbxAlamat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxAlamat.Location = New System.Drawing.Point(9, 86)
        Me.tbxAlamat.Name = "tbxAlamat"
        Me.tbxAlamat.Size = New System.Drawing.Size(185, 64)
        Me.tbxAlamat.TabIndex = 5
        Me.tbxAlamat.Text = ""
        '
        'tbxNamaPegawai
        '
        Me.tbxNamaPegawai.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxNamaPegawai.Location = New System.Drawing.Point(9, 40)
        Me.tbxNamaPegawai.Name = "tbxNamaPegawai"
        Me.tbxNamaPegawai.Size = New System.Drawing.Size(185, 20)
        Me.tbxNamaPegawai.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 211)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Jabatan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 159)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "HP"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Alamat Pegawai"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nama Pegawai"
        '
        'JabatanTableAdapter
        '
        Me.JabatanTableAdapter.ClearBeforeFill = True
        '
        'FPegawai
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 431)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FPegawai"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Pegawai"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvPegawai, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.JabatanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Database_LaundryDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvPegawai As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxJabatan As ComboBox
    Friend WithEvents tbxHP As TextBox
    Friend WithEvents tbxAlamat As RichTextBox
    Friend WithEvents tbxNamaPegawai As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnUbah As Button
    Friend WithEvents btnTambah As Button
    Friend WithEvents Database_LaundryDataSet As Database_LaundryDataSet
    Friend WithEvents JabatanBindingSource As BindingSource
    Friend WithEvents JabatanTableAdapter As Database_LaundryDataSetTableAdapters.JabatanTableAdapter
    Friend WithEvents btnReset As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents cbxStatus As ComboBox
End Class
