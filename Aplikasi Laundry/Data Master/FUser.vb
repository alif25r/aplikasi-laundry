﻿Imports System.Data.OleDb

Public Class FUser
    Private User As New CUser

    Private Sub FUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Database_LaundryDataSet.Roles' table. You can move, or remove it, as needed.
        Me.RolesTableAdapter.Fill(Me.Database_LaundryDataSet.Roles)
        'TODO: This line of code loads data into the 'Database_LaundryDataSet.Pegawai' table. You can move, or remove it, as needed.
        Me.User.Load(dgvUser)

        Me.cbxStatus.SelectedIndex = 0
        Me.btnUbah.Enabled = False

        Me.User.LoadPegawaiNonUser(cbxPegawai)
    End Sub

    Private Sub btnTambah_Click(sender As Object, e As EventArgs) Handles btnTambah.Click
        Me.User.dataIdPegawai = cbxPegawai.SelectedValue
        Me.User.dataUsername = tbxUsername.Text
        Me.User.dataPassword = tbxPassword.Text
        Me.User.dataIdRole = cbxRole.SelectedValue
        Me.User.dataStatus = cbxStatus.SelectedIndex

        If Me.User.dataUsername <> "" And Me.User.dataPassword <> "" Then
            Dim Added = Me.User.Add()

            If Added Then
                MessageBox.Show("Data User Berhasil Ditambahkan", "Form Response")
                Me.User.Load(dgvUser)
                btnReset.PerformClick()
            Else
                MessageBox.Show("Data User Gagal Ditambahkan", "Form Response")
            End If
        Else
            MessageBox.Show("Silakan lengkapi Form User", "Form Validation")
        End If
    End Sub

    Private Sub btnUbah_Click(sender As Object, e As EventArgs) Handles btnUbah.Click
        Me.User.dataUsername = tbxUsername.Text
        If tbxPassword.TextLength > 0 Then
            Me.User.dataPassword = tbxPassword.Text
        End If

        Me.User.dataStatus = cbxStatus.SelectedIndex
        Me.User.dataIdRole = cbxRole.SelectedValue

        If Me.User.dataUsername <> "" Then
            Dim Updated = Me.User.Update()

            If Updated Then
                MessageBox.Show("Data User Berhasil Diubah", "Form Response")
                Me.User.Load(dgvUser)
                btnReset.PerformClick()
            End If
        Else
            MessageBox.Show("Silakan lengkapi form User", "Form Validation")
        End If
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Me.tbxUsername.Text = ""
        Me.tbxPassword.Text = ""
        Me.cbxStatus.SelectedIndex = 0

        Me.User = New CUser
        Me.User.LoadPegawaiNonUser(cbxPegawai)

        Me.btnTambah.Enabled = True
        Me.btnUbah.Enabled = False
        cbxPegawai.Enabled = True
    End Sub

    Private Sub dgvUser_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvUser.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgvUser.Rows(e.RowIndex)

            'Cell Value - Data Pegawai
            Dim id = selectedRow.Cells(0).Value
            Me.User.SetDataById(id)

            Me.tbxUsername.Text = Me.User.dataUsername
            Me.cbxStatus.SelectedIndex = Me.User.dataStatus
            Me.cbxRole.SelectedValue = Me.User.dataIdRole

            btnTambah.Enabled = False
            btnUbah.Enabled = True

            Me.User.LoadPegawaiNonUser(cbxPegawai, Me.User.dataIdPegawai)
            cbxPegawai.Enabled = False
        End If
    End Sub
End Class