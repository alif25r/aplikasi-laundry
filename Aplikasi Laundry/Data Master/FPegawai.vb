﻿Public Class FPegawai
    Private Pegawai As New CPegawai

    Private Sub FPegawai_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Database_LaundryDataSet.Jabatan' table. You can move, or remove it, as needed.
        Me.JabatanTableAdapter.Fill(Me.Database_LaundryDataSet.Jabatan)

        Me.Pegawai.Load(dgvPegawai)

        Me.cbxStatus.SelectedIndex = 0
    End Sub

    Private Sub dgvPegawai_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvPegawai.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgvPegawai.Rows(e.RowIndex)

            'Cell Value - Data Pegawai
            Dim id = selectedRow.Cells(0).Value
            Me.Pegawai.SetDataById(id)

            Me.tbxNamaPegawai.Text = Me.Pegawai.dataNama
            Me.tbxAlamat.Text = Me.Pegawai.dataAlamat
            Me.tbxHP.Text = Me.Pegawai.dataTelp
            Me.cbxJabatan.Text = Me.Pegawai.dataJabatan
            Me.cbxStatus.SelectedIndex = Me.Pegawai.dataStatus

            btnTambah.Enabled = False
            btnUbah.Enabled = True
        End If
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Me.tbxNamaPegawai.Text = ""
        Me.tbxAlamat.Text = ""
        Me.tbxHP.Text = ""

        Me.Pegawai = New CPegawai

        btnTambah.Enabled = True
        btnUbah.Enabled = False
    End Sub

    Private Sub btnTambah_Click(sender As Object, e As EventArgs) Handles btnTambah.Click
        Me.Pegawai.dataNama = tbxNamaPegawai.Text
        Me.Pegawai.dataAlamat = tbxAlamat.Text
        Me.Pegawai.dataIdJabatan = cbxJabatan.SelectedValue
        Me.Pegawai.dataTelp = tbxHP.Text

        If (Me.Pegawai.dataNama <> "" And Me.Pegawai.dataAlamat <> "" And Me.Pegawai.dataTelp <> "") Then
            Dim resultAdd = Me.Pegawai.Add()

            If resultAdd Then
                MessageBox.Show("Data Pegawai Berhasil Ditambahkan", "Form Response")
                Me.Pegawai.Load(dgvPegawai)
                btnReset.PerformClick()
            End If
        Else
            MessageBox.Show("Silakan lengkapi form pegawai", "Form Validation")
        End If
    End Sub

    Private Sub btnUbah_Click(sender As Object, e As EventArgs) Handles btnUbah.Click
        Me.Pegawai.dataNama = tbxNamaPegawai.Text
        Me.Pegawai.dataAlamat = tbxAlamat.Text
        Me.Pegawai.dataIdJabatan = cbxJabatan.SelectedValue
        Me.Pegawai.dataTelp = tbxHP.Text
        Me.Pegawai.dataStatus = cbxStatus.SelectedIndex

        If (Me.Pegawai.dataNama <> "" And Me.Pegawai.dataAlamat <> "" And Me.Pegawai.dataTelp <> "") Then
            Dim resultAdd = Me.Pegawai.Edit()

            If resultAdd Then
                MessageBox.Show("Data Pegawai Berhasil Diubah", "Form Response")
                Me.Pegawai.Load(dgvPegawai)
                btnReset.PerformClick()
            End If
        Else
            MessageBox.Show("Silakan lengkapi form pegawai", "Form Validation")
        End If
    End Sub
End Class