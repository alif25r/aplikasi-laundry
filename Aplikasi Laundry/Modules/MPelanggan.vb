﻿Imports System.Data.OleDb

Module MPelanggan
    Public Function ListNamaPelanggan() As AutoCompleteStringCollection
        Dim PelangganList As New AutoCompleteStringCollection

        KoneksiDB()

        str = "select nama_pelanggan from Pelanggan"
        cmd = New OleDbCommand(str, conn)
        dr = cmd.ExecuteReader

        If dr.HasRows Then
            While dr.Read()
                PelangganList.Add(dr.GetString(0).Trim().ToString())
            End While
        End If

        dr.Close()

        KoneksiDB(True)

        Return PelangganList
    End Function

    Public Sub PelangganTbxAutocomplete(ByVal txt As TextBox)
        With txt
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = ListNamaPelanggan()
        End With
    End Sub
End Module
