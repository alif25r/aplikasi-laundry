﻿Imports System.Data.OleDb

Public Class FTambahCucian
    Public dataUser As CUser

    Private dataPelanggan As CPelanggan

    Private Sub FTambahCucian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Database_LaundryDataSet.Pelanggan' table. You can move, or remove it, as needed.
        Me.PelangganTableAdapter.Fill(Me.Database_LaundryDataSet.Pelanggan)
        'TODO: This line of code loads data into the 'Database_LaundryDataSet.Jenis_Cucian' table. You can move, or remove it, as needed.
        Me.Jenis_CucianTableAdapter.Fill(Me.Database_LaundryDataSet.Jenis_Cucian)
        PelangganTbxAutocomplete(PelangganTextBox)

        Me.JenisCbx_SelectedIndexChanged(sender, e)

        Me.dataPelanggan = New CPelanggan
    End Sub

    Private Sub JenisCbx_SelectedIndexChanged(sender As Object, e As EventArgs) Handles JenisCbx.SelectedIndexChanged
        Dim id_jenis = JenisCbx.SelectedValue

        If Not String.IsNullOrEmpty(id_jenis) Then
            KoneksiDB()

            str = "select * from Jenis_Cucian where id_jenis_cucian = " & id_jenis

            cmd = New OleDbCommand(str, conn)
            dr = cmd.ExecuteReader

            If dr.HasRows Then
                dr.Read()
                SatuanTextBox.Text = dr.GetValue(dr.GetOrdinal("satuan"))
                HargaTextBox.Text = dr.GetValue(dr.GetOrdinal("harga"))
            End If

            dr.Close()
            If (Not String.IsNullOrEmpty(JumlahTextBox.Text) And IsNumeric(JumlahTextBox.Text)) Then
                TotalTextBox.Text = CInt(HargaTextBox.Text) * CDec(JumlahTextBox.Text)
            End If

            KoneksiDB(True)
        End If
    End Sub

    Private Sub JumlahTextBox_TextChanged(sender As Object, e As EventArgs) Handles JumlahTextBox.TextChanged
        If Not IsNumeric(JumlahTextBox.Text) And IsNothing(JumlahTextBox.Text) Then
            MessageBox.Show("only numbers allowed")
            JumlahTextBox.Text = ""
        Else
            If Not String.IsNullOrEmpty(JumlahTextBox.Text) Then
                TotalTextBox.Text = Val(HargaTextBox.Text) * Val(JumlahTextBox.Text)
            End If
        End If
    End Sub

    Private Sub PelangganTextBox_Leave(sender As Object, e As EventArgs) Handles PelangganTextBox.Leave
        Dim nama_pelanggan = PelangganTextBox.Text

        If Not String.IsNullOrEmpty(nama_pelanggan) Then
            If Me.dataPelanggan.GetPelangganByName(nama_pelanggan) Then
                NoTelpPelangganTbx.Text = Me.dataPelanggan.VNoTelpPelanggan
                AlamatPelangganTbx.Text = Me.dataPelanggan.VAlamatPelanggan
                NoTelpPelangganTbx.ReadOnly = True
                AlamatPelangganTbx.ReadOnly = True
            End If
        End If
    End Sub

    Private Sub PelangganTextBox_TextChanged(sender As Object, e As EventArgs) Handles PelangganTextBox.TextChanged
        NoTelpPelangganTbx.Text = ""
        AlamatPelangganTbx.Text = ""
        NoTelpPelangganTbx.ReadOnly = False
        AlamatPelangganTbx.ReadOnly = False
    End Sub

    Private Sub TambahCucianButton_Click(sender As Object, e As EventArgs) Handles TambahCucianButton.Click
        Dim Cucian As New CCucian With {
            .IdPegawai = dataUser.dataId,
            .IdPelanggan = Me.dataPelanggan.VIdPelanggan,
            .IdJenisCucian = JenisCbx.SelectedValue,
            .Jumlah = JumlahTextBox.Text,
            .TglMasuk = Now(),
            .TotalHarga = TotalTextBox.Text
        }
        Dim result = Cucian.SimpanCucian()

        If result Then
            Me.Close()
        End If
    End Sub
End Class