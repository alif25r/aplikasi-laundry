﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FCucian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCucian))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TambahButton = New System.Windows.Forms.Button()
        Me.CucianFilterBox = New System.Windows.Forms.GroupBox()
        Me.CheckBoxKeluar = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMasuk = New System.Windows.Forms.CheckBox()
        Me.DateSAkhir = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DateSAwal = New System.Windows.Forms.DateTimePicker()
        Me.DateMAkhir = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DateMAwal = New System.Windows.Forms.DateTimePicker()
        Me.fakturTbx = New System.Windows.Forms.TextBox()
        Me.konsumenTbx = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FilterButton = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.dateTimeLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.dgvCucian = New System.Windows.Forms.DataGridView()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.CucianFilterBox.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgvCucian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TambahButton
        '
        Me.TambahButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TambahButton.BackgroundImage = CType(resources.GetObject("TambahButton.BackgroundImage"), System.Drawing.Image)
        Me.TambahButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.TambahButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TambahButton.Location = New System.Drawing.Point(746, 12)
        Me.TambahButton.Name = "TambahButton"
        Me.TambahButton.Size = New System.Drawing.Size(46, 46)
        Me.TambahButton.TabIndex = 2
        Me.TambahButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.TambahButton.UseVisualStyleBackColor = True
        '
        'CucianFilterBox
        '
        Me.CucianFilterBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CucianFilterBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CucianFilterBox.Controls.Add(Me.CheckBoxKeluar)
        Me.CucianFilterBox.Controls.Add(Me.CheckBoxMasuk)
        Me.CucianFilterBox.Controls.Add(Me.DateSAkhir)
        Me.CucianFilterBox.Controls.Add(Me.Label6)
        Me.CucianFilterBox.Controls.Add(Me.DateSAwal)
        Me.CucianFilterBox.Controls.Add(Me.DateMAkhir)
        Me.CucianFilterBox.Controls.Add(Me.Label4)
        Me.CucianFilterBox.Controls.Add(Me.DateMAwal)
        Me.CucianFilterBox.Controls.Add(Me.fakturTbx)
        Me.CucianFilterBox.Controls.Add(Me.konsumenTbx)
        Me.CucianFilterBox.Controls.Add(Me.Label2)
        Me.CucianFilterBox.Controls.Add(Me.Label1)
        Me.CucianFilterBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CucianFilterBox.Location = New System.Drawing.Point(12, 12)
        Me.CucianFilterBox.Name = "CucianFilterBox"
        Me.CucianFilterBox.Size = New System.Drawing.Size(728, 117)
        Me.CucianFilterBox.TabIndex = 1
        Me.CucianFilterBox.TabStop = False
        Me.CucianFilterBox.Text = "Filter Data"
        '
        'CheckBoxKeluar
        '
        Me.CheckBoxKeluar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxKeluar.AutoSize = True
        Me.CheckBoxKeluar.Location = New System.Drawing.Point(485, 66)
        Me.CheckBoxKeluar.Name = "CheckBoxKeluar"
        Me.CheckBoxKeluar.Size = New System.Drawing.Size(93, 17)
        Me.CheckBoxKeluar.TabIndex = 15
        Me.CheckBoxKeluar.Text = "Tgl. Selesai"
        Me.CheckBoxKeluar.UseVisualStyleBackColor = True
        '
        'CheckBoxMasuk
        '
        Me.CheckBoxMasuk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxMasuk.AutoSize = True
        Me.CheckBoxMasuk.Location = New System.Drawing.Point(485, 20)
        Me.CheckBoxMasuk.Name = "CheckBoxMasuk"
        Me.CheckBoxMasuk.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxMasuk.TabIndex = 14
        Me.CheckBoxMasuk.Text = "Tgl. Masuk"
        Me.CheckBoxMasuk.UseVisualStyleBackColor = True
        '
        'DateSAkhir
        '
        Me.DateSAkhir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateSAkhir.CustomFormat = "dd-MM-yyyy"
        Me.DateSAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateSAkhir.Location = New System.Drawing.Point(612, 86)
        Me.DateSAkhir.Name = "DateSAkhir"
        Me.DateSAkhir.Size = New System.Drawing.Size(103, 20)
        Me.DateSAkhir.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(595, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "-"
        '
        'DateSAwal
        '
        Me.DateSAwal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateSAwal.CustomFormat = "dd-MM-yyyy"
        Me.DateSAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateSAwal.Location = New System.Drawing.Point(485, 86)
        Me.DateSAwal.Name = "DateSAwal"
        Me.DateSAwal.Size = New System.Drawing.Size(104, 20)
        Me.DateSAwal.TabIndex = 11
        '
        'DateMAkhir
        '
        Me.DateMAkhir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateMAkhir.CustomFormat = "dd-MM-yyyy"
        Me.DateMAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateMAkhir.Location = New System.Drawing.Point(612, 39)
        Me.DateMAkhir.Name = "DateMAkhir"
        Me.DateMAkhir.Size = New System.Drawing.Size(103, 20)
        Me.DateMAkhir.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(595, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "-"
        '
        'DateMAwal
        '
        Me.DateMAwal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateMAwal.CustomFormat = "dd-MM-yyyy"
        Me.DateMAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateMAwal.Location = New System.Drawing.Point(485, 39)
        Me.DateMAwal.Name = "DateMAwal"
        Me.DateMAwal.Size = New System.Drawing.Size(104, 20)
        Me.DateMAwal.TabIndex = 5
        '
        'fakturTbx
        '
        Me.fakturTbx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fakturTbx.Location = New System.Drawing.Point(10, 85)
        Me.fakturTbx.Name = "fakturTbx"
        Me.fakturTbx.Size = New System.Drawing.Size(454, 20)
        Me.fakturTbx.TabIndex = 3
        '
        'konsumenTbx
        '
        Me.konsumenTbx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.konsumenTbx.Location = New System.Drawing.Point(10, 39)
        Me.konsumenTbx.Name = "konsumenTbx"
        Me.konsumenTbx.Size = New System.Drawing.Size(454, 20)
        Me.konsumenTbx.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No. Faktur"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Konsumen"
        '
        'FilterButton
        '
        Me.FilterButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FilterButton.Location = New System.Drawing.Point(746, 64)
        Me.FilterButton.Name = "FilterButton"
        Me.FilterButton.Size = New System.Drawing.Size(99, 65)
        Me.FilterButton.TabIndex = 4
        Me.FilterButton.Text = "Tampilkan"
        Me.FilterButton.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgressBar, Me.dateTimeLabel})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 469)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(857, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ProgressBar
        '
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(100, 16)
        Me.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'dateTimeLabel
        '
        Me.dateTimeLabel.BackColor = System.Drawing.SystemColors.Menu
        Me.dateTimeLabel.Name = "dateTimeLabel"
        Me.dateTimeLabel.Size = New System.Drawing.Size(60, 17)
        Me.dateTimeLabel.Text = "Date Time"
        '
        'dgvCucian
        '
        Me.dgvCucian.AllowUserToAddRows = False
        Me.dgvCucian.AllowUserToDeleteRows = False
        Me.dgvCucian.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCucian.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCucian.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Me.dgvCucian.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgvCucian.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCucian.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCucian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCucian.Location = New System.Drawing.Point(12, 135)
        Me.dgvCucian.Name = "dgvCucian"
        Me.dgvCucian.ReadOnly = True
        Me.dgvCucian.RowHeadersVisible = False
        Me.dgvCucian.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCucian.ShowEditingIcon = False
        Me.dgvCucian.Size = New System.Drawing.Size(833, 331)
        Me.dgvCucian.TabIndex = 3
        '
        'Timer
        '
        '
        'RefreshButton
        '
        Me.RefreshButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RefreshButton.BackgroundImage = CType(resources.GetObject("RefreshButton.BackgroundImage"), System.Drawing.Image)
        Me.RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.RefreshButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshButton.Location = New System.Drawing.Point(799, 12)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(46, 46)
        Me.RefreshButton.TabIndex = 6
        Me.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.RefreshButton.UseVisualStyleBackColor = True
        '
        'FCucian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(857, 491)
        Me.Controls.Add(Me.RefreshButton)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.FilterButton)
        Me.Controls.Add(Me.CucianFilterBox)
        Me.Controls.Add(Me.TambahButton)
        Me.Controls.Add(Me.dgvCucian)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(873, 530)
        Me.Name = "FCucian"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Cucian"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.CucianFilterBox.ResumeLayout(False)
        Me.CucianFilterBox.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgvCucian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TambahButton As System.Windows.Forms.Button
    Friend WithEvents CucianFilterBox As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents fakturTbx As TextBox
    Friend WithEvents konsumenTbx As TextBox
    Friend WithEvents DateMAwal As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents DateMAkhir As DateTimePicker
    Friend WithEvents FilterButton As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ProgressBar As ToolStripProgressBar
    Friend WithEvents dgvCucian As DataGridView
    Friend WithEvents DateSAkhir As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents DateSAwal As DateTimePicker
    Friend WithEvents dateTimeLabel As ToolStripStatusLabel
    Friend WithEvents Timer As Timer
    Friend WithEvents RefreshButton As Button
    Friend WithEvents CheckBoxKeluar As CheckBox
    Friend WithEvents CheckBoxMasuk As CheckBox
End Class
