﻿Imports System.Globalization

Public Class FAmbilCucian
    Public dataUser As CUser

    Private Cucian As New CCucian

    Private Sub FAmbilCucian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dateTimeLabel.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
        Timer.Start()

        Me.Cucian.LoadCucian(dgvCucian, True)
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        Me.Cucian.LoadCucian(dgvCucian, True)
    End Sub

    Private Sub FilterButton_Click(sender As Object, e As EventArgs) Handles FilterButton.Click
        Dim nama_pelanggan = konsumenTbx.Text
        Dim no_faktur = fakturTbx.Text
        Dim tgl_masuk_awal = DateMAwal.Value
        Dim tgl_masuk_akhir = DateMAkhir.Value
        Dim tgl_keluar_awal = DateKAwal.Value
        Dim tgl_keluar_akhir = DateKAkhir.Value

        Dim params As New List(Of String)

        If nama_pelanggan <> "" Then
            params.Add("nama_pelanggan like '%" + nama_pelanggan + "%'")
        End If

        If no_faktur <> "" Then
            params.Add("no_faktur like '%" + no_faktur + "%'")
        End If

        If CheckBoxMasuk.Checked Then
            params.Add("tgl_masuk Between #" + tgl_masuk_awal + "# and #" + tgl_masuk_akhir + "#")
        End If

        If CheckBoxKeluar.Checked Then
            params.Add("tgl_keluar Between #" + tgl_keluar_awal + "# and #" + tgl_keluar_akhir + "#")
        End If

        Me.Cucian.FilterCucian(dgvCucian, params, True)
    End Sub

    Private Sub dgvCucian_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCucian.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgvCucian.Rows(e.RowIndex)

            'Cell Value - Data Cucian
            Dim id = selectedRow.Cells(0).Value
            Dim nama_pelanggan = selectedRow.Cells(2).Value
            Dim jenis_cucian = selectedRow.Cells(4).Value
            Dim jumlah = selectedRow.Cells(5).Value
            Dim harga = selectedRow.Cells(6).Value
            Dim total = selectedRow.Cells(7).Value

            Dim cucianKeluarWindow As New FCucianKeluar With {
                .dataUser = Me.dataUser
            }
            cucianKeluarWindow.IDTextBox.Text = id
            cucianKeluarWindow.PelangganTextBox.Text = nama_pelanggan
            cucianKeluarWindow.JenisCucianTextBox.Text = jenis_cucian
            cucianKeluarWindow.JumlahTextBox.Text = jumlah
            cucianKeluarWindow.HargaTextBox.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,##}", harga)
            cucianKeluarWindow.TotalTextBox.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,##}", total)
            cucianKeluarWindow.ShowDialog()

            Me.Cucian.LoadCucian(dgvCucian, True)
        End If
    End Sub
End Class