﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FTambahCucian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Id_pelangganLabel As System.Windows.Forms.Label
        Dim Id_jenis_cucianLabel As System.Windows.Forms.Label
        Dim JumlahLabel As System.Windows.Forms.Label
        Dim SatuanLabel As System.Windows.Forms.Label
        Dim HargaLabel As System.Windows.Forms.Label
        Dim Total_hargaLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FTambahCucian))
        Me.PelangganTextBox = New System.Windows.Forms.TextBox()
        Me.JumlahTextBox = New System.Windows.Forms.TextBox()
        Me.SatuanTextBox = New System.Windows.Forms.TextBox()
        Me.HargaTextBox = New System.Windows.Forms.TextBox()
        Me.TotalTextBox = New System.Windows.Forms.TextBox()
        Me.TambahCucianButton = New System.Windows.Forms.Button()
        Me.JenisCbx = New System.Windows.Forms.ComboBox()
        Me.JenisCucianBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Database_LaundryDataSet = New WindowsApplication1.Database_LaundryDataSet()
        Me.Jenis_CucianTableAdapter = New WindowsApplication1.Database_LaundryDataSetTableAdapters.Jenis_CucianTableAdapter()
        Me.NoTelpPelangganTbx = New System.Windows.Forms.TextBox()
        Me.AlamatPelangganTbx = New System.Windows.Forms.TextBox()
        Me.PelangganBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PelangganTableAdapter = New WindowsApplication1.Database_LaundryDataSetTableAdapters.PelangganTableAdapter()
        Id_pelangganLabel = New System.Windows.Forms.Label()
        Id_jenis_cucianLabel = New System.Windows.Forms.Label()
        JumlahLabel = New System.Windows.Forms.Label()
        SatuanLabel = New System.Windows.Forms.Label()
        HargaLabel = New System.Windows.Forms.Label()
        Total_hargaLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.JenisCucianBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Database_LaundryDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PelangganBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Id_pelangganLabel
        '
        Id_pelangganLabel.AutoSize = True
        Id_pelangganLabel.Location = New System.Drawing.Point(12, 11)
        Id_pelangganLabel.Name = "Id_pelangganLabel"
        Id_pelangganLabel.Size = New System.Drawing.Size(58, 13)
        Id_pelangganLabel.TabIndex = 7
        Id_pelangganLabel.Text = "Pelanggan"
        '
        'Id_jenis_cucianLabel
        '
        Id_jenis_cucianLabel.AutoSize = True
        Id_jenis_cucianLabel.Location = New System.Drawing.Point(12, 90)
        Id_jenis_cucianLabel.Name = "Id_jenis_cucianLabel"
        Id_jenis_cucianLabel.Size = New System.Drawing.Size(67, 13)
        Id_jenis_cucianLabel.TabIndex = 9
        Id_jenis_cucianLabel.Text = "Jenis Cucian"
        '
        'JumlahLabel
        '
        JumlahLabel.AutoSize = True
        JumlahLabel.Location = New System.Drawing.Point(12, 142)
        JumlahLabel.Name = "JumlahLabel"
        JumlahLabel.Size = New System.Drawing.Size(40, 13)
        JumlahLabel.TabIndex = 11
        JumlahLabel.Text = "Jumlah"
        '
        'SatuanLabel
        '
        SatuanLabel.AutoSize = True
        SatuanLabel.Location = New System.Drawing.Point(12, 116)
        SatuanLabel.Name = "SatuanLabel"
        SatuanLabel.Size = New System.Drawing.Size(41, 13)
        SatuanLabel.TabIndex = 13
        SatuanLabel.Text = "Satuan"
        '
        'HargaLabel
        '
        HargaLabel.AutoSize = True
        HargaLabel.Location = New System.Drawing.Point(12, 168)
        HargaLabel.Name = "HargaLabel"
        HargaLabel.Size = New System.Drawing.Size(36, 13)
        HargaLabel.TabIndex = 15
        HargaLabel.Text = "Harga"
        '
        'Total_hargaLabel
        '
        Total_hargaLabel.AutoSize = True
        Total_hargaLabel.Location = New System.Drawing.Point(12, 194)
        Total_hargaLabel.Name = "Total_hargaLabel"
        Total_hargaLabel.Size = New System.Drawing.Size(31, 13)
        Total_hargaLabel.TabIndex = 17
        Total_hargaLabel.Text = "Total"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(12, 37)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(48, 13)
        Label1.TabIndex = 27
        Label1.Text = "No Telp."
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(12, 63)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(60, 13)
        Label2.TabIndex = 29
        Label2.Text = "Alamat Plg."
        '
        'PelangganTextBox
        '
        Me.PelangganTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PelangganTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.PelangganTextBox.Location = New System.Drawing.Point(95, 8)
        Me.PelangganTextBox.Name = "PelangganTextBox"
        Me.PelangganTextBox.Size = New System.Drawing.Size(200, 20)
        Me.PelangganTextBox.TabIndex = 8
        '
        'JumlahTextBox
        '
        Me.JumlahTextBox.Location = New System.Drawing.Point(95, 139)
        Me.JumlahTextBox.Name = "JumlahTextBox"
        Me.JumlahTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JumlahTextBox.TabIndex = 12
        Me.JumlahTextBox.Text = "0"
        '
        'SatuanTextBox
        '
        Me.SatuanTextBox.Location = New System.Drawing.Point(95, 113)
        Me.SatuanTextBox.Name = "SatuanTextBox"
        Me.SatuanTextBox.ReadOnly = True
        Me.SatuanTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SatuanTextBox.TabIndex = 14
        Me.SatuanTextBox.Text = "-"
        '
        'HargaTextBox
        '
        Me.HargaTextBox.Location = New System.Drawing.Point(95, 165)
        Me.HargaTextBox.Name = "HargaTextBox"
        Me.HargaTextBox.ReadOnly = True
        Me.HargaTextBox.Size = New System.Drawing.Size(200, 20)
        Me.HargaTextBox.TabIndex = 16
        Me.HargaTextBox.Text = "0"
        '
        'TotalTextBox
        '
        Me.TotalTextBox.Location = New System.Drawing.Point(95, 191)
        Me.TotalTextBox.Name = "TotalTextBox"
        Me.TotalTextBox.ReadOnly = True
        Me.TotalTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TotalTextBox.TabIndex = 18
        Me.TotalTextBox.Text = "0"
        '
        'TambahCucianButton
        '
        Me.TambahCucianButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TambahCucianButton.Location = New System.Drawing.Point(15, 217)
        Me.TambahCucianButton.Name = "TambahCucianButton"
        Me.TambahCucianButton.Size = New System.Drawing.Size(280, 37)
        Me.TambahCucianButton.TabIndex = 25
        Me.TambahCucianButton.Text = "TAMBAH"
        Me.TambahCucianButton.UseVisualStyleBackColor = True
        '
        'JenisCbx
        '
        Me.JenisCbx.DataSource = Me.JenisCucianBindingSource
        Me.JenisCbx.DisplayMember = "jenis_cucian"
        Me.JenisCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.JenisCbx.FormattingEnabled = True
        Me.JenisCbx.Location = New System.Drawing.Point(95, 86)
        Me.JenisCbx.Name = "JenisCbx"
        Me.JenisCbx.Size = New System.Drawing.Size(200, 21)
        Me.JenisCbx.TabIndex = 26
        Me.JenisCbx.ValueMember = "id_jenis_cucian"
        '
        'JenisCucianBindingSource
        '
        Me.JenisCucianBindingSource.DataMember = "Jenis_Cucian"
        Me.JenisCucianBindingSource.DataSource = Me.Database_LaundryDataSet
        '
        'Database_LaundryDataSet
        '
        Me.Database_LaundryDataSet.DataSetName = "Database_LaundryDataSet"
        Me.Database_LaundryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Jenis_CucianTableAdapter
        '
        Me.Jenis_CucianTableAdapter.ClearBeforeFill = True
        '
        'NoTelpPelangganTbx
        '
        Me.NoTelpPelangganTbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.NoTelpPelangganTbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.NoTelpPelangganTbx.Location = New System.Drawing.Point(95, 34)
        Me.NoTelpPelangganTbx.Name = "NoTelpPelangganTbx"
        Me.NoTelpPelangganTbx.Size = New System.Drawing.Size(200, 20)
        Me.NoTelpPelangganTbx.TabIndex = 28
        '
        'AlamatPelangganTbx
        '
        Me.AlamatPelangganTbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.AlamatPelangganTbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.AlamatPelangganTbx.Location = New System.Drawing.Point(95, 60)
        Me.AlamatPelangganTbx.Name = "AlamatPelangganTbx"
        Me.AlamatPelangganTbx.Size = New System.Drawing.Size(200, 20)
        Me.AlamatPelangganTbx.TabIndex = 30
        '
        'PelangganBindingSource
        '
        Me.PelangganBindingSource.DataMember = "Pelanggan"
        Me.PelangganBindingSource.DataSource = Me.Database_LaundryDataSet
        '
        'PelangganTableAdapter
        '
        Me.PelangganTableAdapter.ClearBeforeFill = True
        '
        'FTambahCucian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(309, 261)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.AlamatPelangganTbx)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.NoTelpPelangganTbx)
        Me.Controls.Add(Me.JenisCbx)
        Me.Controls.Add(Me.TambahCucianButton)
        Me.Controls.Add(Id_pelangganLabel)
        Me.Controls.Add(Me.PelangganTextBox)
        Me.Controls.Add(Id_jenis_cucianLabel)
        Me.Controls.Add(JumlahLabel)
        Me.Controls.Add(Me.JumlahTextBox)
        Me.Controls.Add(SatuanLabel)
        Me.Controls.Add(Me.SatuanTextBox)
        Me.Controls.Add(HargaLabel)
        Me.Controls.Add(Me.HargaTextBox)
        Me.Controls.Add(Total_hargaLabel)
        Me.Controls.Add(Me.TotalTextBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FTambahCucian"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tambah Cucian"
        CType(Me.JenisCucianBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Database_LaundryDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PelangganBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PelangganTextBox As TextBox
    Friend WithEvents JumlahTextBox As TextBox
    Friend WithEvents SatuanTextBox As TextBox
    Friend WithEvents HargaTextBox As TextBox
    Friend WithEvents TotalTextBox As TextBox
    Friend WithEvents TambahCucianButton As Button
    Friend WithEvents JenisCbx As ComboBox
    Friend WithEvents Database_LaundryDataSet As Database_LaundryDataSet
    Friend WithEvents JenisCucianBindingSource As BindingSource
    Friend WithEvents Jenis_CucianTableAdapter As Database_LaundryDataSetTableAdapters.Jenis_CucianTableAdapter
    Friend WithEvents NoTelpPelangganTbx As TextBox
    Friend WithEvents AlamatPelangganTbx As TextBox
    Friend WithEvents PelangganBindingSource As BindingSource
    Friend WithEvents PelangganTableAdapter As Database_LaundryDataSetTableAdapters.PelangganTableAdapter
End Class
