﻿Imports System.Data.OleDb
Imports System.Globalization

Public Class FCucian
    Public dataUser As CUser

    Private Cucian As New CCucian

    Private Sub FCucian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dateTimeLabel.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
        Timer.Start()

        Me.Cucian.LoadCucian(dgvCucian)
    End Sub

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick
        dateTimeLabel.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
    End Sub

    Private Sub TambahButton_Click(sender As Object, e As EventArgs) Handles TambahButton.Click
        Dim FTambahC As New FTambahCucian With {
            .dataUser = Me.dataUser
        }
        FTambahC.ShowDialog()

        Me.Cucian.LoadCucian(dgvCucian)
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        Me.Cucian.LoadCucian(dgvCucian)
    End Sub

    Private Sub FilterButton_Click(sender As Object, e As EventArgs) Handles FilterButton.Click
        Dim nama_pelanggan = konsumenTbx.Text
        Dim no_faktur = fakturTbx.Text
        Dim tgl_masuk_awal = DateMAwal.Value
        Dim tgl_masuk_akhir = DateMAkhir.Value
        Dim tgl_selesai_awal = DateSAwal.Value
        Dim tgl_selesai_akhir = DateSAkhir.Value

        Dim params As New List(Of String)

        If nama_pelanggan <> "" Then
            params.Add("nama_pelanggan like '%" + nama_pelanggan + "%'")
        End If

        If no_faktur <> "" Then
            params.Add("no_faktur like '%" + no_faktur + "%'")
        End If

        If CheckBoxMasuk.Checked Then
            params.Add("tgl_masuk Between #" + tgl_masuk_awal + "# and #" + tgl_masuk_akhir + "#")
        End If

        If CheckBoxKeluar.Checked Then
            params.Add("tgl_selesai Between #" + tgl_selesai_awal + "# and #" + tgl_selesai_akhir + "#")
        End If

        Me.Cucian.FilterCucian(dgvCucian, params)
    End Sub

    Private Sub dgvCucian_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCucian.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgvCucian.Rows(e.RowIndex)

            'Cell Value - Data Cucian
            Dim id = selectedRow.Cells(0).Value
            Dim nama_pelanggan = selectedRow.Cells(2).Value
            Dim jenis_cucian = selectedRow.Cells(4).Value
            Dim jumlah = selectedRow.Cells(5).Value
            Dim harga = selectedRow.Cells(6).Value
            Dim total = selectedRow.Cells(7).Value

            Dim cucianSelesaiWindow As New FCucianSelesai With {
                .dataUser = Me.dataUser
            }
            cucianSelesaiWindow.IDTextBox.Text = id
            cucianSelesaiWindow.PelangganTextBox.Text = nama_pelanggan
            cucianSelesaiWindow.JenisCucianTextBox.Text = jenis_cucian
            cucianSelesaiWindow.JumlahTextBox.Text = jumlah
            cucianSelesaiWindow.HargaTextBox.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,##}", harga)
            cucianSelesaiWindow.TotalTextBox.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,##}", total)
            cucianSelesaiWindow.ShowDialog()

            Me.Cucian.LoadCucian(dgvCucian)
        End If
    End Sub
End Class