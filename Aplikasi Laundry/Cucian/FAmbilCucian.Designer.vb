﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FAmbilCucian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FAmbilCucian))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CheckBoxKeluar = New System.Windows.Forms.CheckBox()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.CheckBoxMasuk = New System.Windows.Forms.CheckBox()
        Me.DateKAkhir = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DateKAwal = New System.Windows.Forms.DateTimePicker()
        Me.DateMAkhir = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DateMAwal = New System.Windows.Forms.DateTimePicker()
        Me.fakturTbx = New System.Windows.Forms.TextBox()
        Me.konsumenTbx = New System.Windows.Forms.TextBox()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.dgvCucian = New System.Windows.Forms.DataGridView()
        Me.dateTimeLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.FilterButton = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CucianFilterBox = New System.Windows.Forms.GroupBox()
        CType(Me.dgvCucian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.CucianFilterBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'CheckBoxKeluar
        '
        Me.CheckBoxKeluar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxKeluar.AutoSize = True
        Me.CheckBoxKeluar.Location = New System.Drawing.Point(485, 66)
        Me.CheckBoxKeluar.Name = "CheckBoxKeluar"
        Me.CheckBoxKeluar.Size = New System.Drawing.Size(88, 17)
        Me.CheckBoxKeluar.TabIndex = 15
        Me.CheckBoxKeluar.Text = "Tgl. Keluar"
        Me.CheckBoxKeluar.UseVisualStyleBackColor = True
        '
        'RefreshButton
        '
        Me.RefreshButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RefreshButton.BackgroundImage = CType(resources.GetObject("RefreshButton.BackgroundImage"), System.Drawing.Image)
        Me.RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.RefreshButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshButton.Location = New System.Drawing.Point(746, 6)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(99, 69)
        Me.RefreshButton.TabIndex = 12
        Me.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.RefreshButton.UseVisualStyleBackColor = True
        '
        'CheckBoxMasuk
        '
        Me.CheckBoxMasuk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxMasuk.AutoSize = True
        Me.CheckBoxMasuk.Location = New System.Drawing.Point(485, 20)
        Me.CheckBoxMasuk.Name = "CheckBoxMasuk"
        Me.CheckBoxMasuk.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxMasuk.TabIndex = 14
        Me.CheckBoxMasuk.Text = "Tgl. Masuk"
        Me.CheckBoxMasuk.UseVisualStyleBackColor = True
        '
        'DateKAkhir
        '
        Me.DateKAkhir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateKAkhir.CustomFormat = "dd-MM-yyyy"
        Me.DateKAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateKAkhir.Location = New System.Drawing.Point(612, 86)
        Me.DateKAkhir.Name = "DateKAkhir"
        Me.DateKAkhir.Size = New System.Drawing.Size(103, 20)
        Me.DateKAkhir.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(595, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "-"
        '
        'DateKAwal
        '
        Me.DateKAwal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateKAwal.CustomFormat = "dd-MM-yyyy"
        Me.DateKAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateKAwal.Location = New System.Drawing.Point(485, 86)
        Me.DateKAwal.Name = "DateKAwal"
        Me.DateKAwal.Size = New System.Drawing.Size(104, 20)
        Me.DateKAwal.TabIndex = 11
        '
        'DateMAkhir
        '
        Me.DateMAkhir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateMAkhir.CustomFormat = "dd-MM-yyyy"
        Me.DateMAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateMAkhir.Location = New System.Drawing.Point(612, 39)
        Me.DateMAkhir.Name = "DateMAkhir"
        Me.DateMAkhir.Size = New System.Drawing.Size(103, 20)
        Me.DateMAkhir.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(595, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "-"
        '
        'DateMAwal
        '
        Me.DateMAwal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateMAwal.CustomFormat = "dd-MM-yyyy"
        Me.DateMAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateMAwal.Location = New System.Drawing.Point(485, 39)
        Me.DateMAwal.Name = "DateMAwal"
        Me.DateMAwal.Size = New System.Drawing.Size(104, 20)
        Me.DateMAwal.TabIndex = 5
        '
        'fakturTbx
        '
        Me.fakturTbx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fakturTbx.Location = New System.Drawing.Point(10, 85)
        Me.fakturTbx.Name = "fakturTbx"
        Me.fakturTbx.Size = New System.Drawing.Size(454, 20)
        Me.fakturTbx.TabIndex = 3
        '
        'konsumenTbx
        '
        Me.konsumenTbx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.konsumenTbx.Location = New System.Drawing.Point(10, 39)
        Me.konsumenTbx.Name = "konsumenTbx"
        Me.konsumenTbx.Size = New System.Drawing.Size(454, 20)
        Me.konsumenTbx.TabIndex = 2
        '
        'dgvCucian
        '
        Me.dgvCucian.AllowUserToAddRows = False
        Me.dgvCucian.AllowUserToDeleteRows = False
        Me.dgvCucian.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCucian.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCucian.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Me.dgvCucian.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgvCucian.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCucian.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCucian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCucian.Location = New System.Drawing.Point(12, 129)
        Me.dgvCucian.Name = "dgvCucian"
        Me.dgvCucian.ReadOnly = True
        Me.dgvCucian.RowHeadersVisible = False
        Me.dgvCucian.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCucian.ShowEditingIcon = False
        Me.dgvCucian.Size = New System.Drawing.Size(833, 331)
        Me.dgvCucian.TabIndex = 9
        '
        'dateTimeLabel
        '
        Me.dateTimeLabel.BackColor = System.Drawing.SystemColors.Menu
        Me.dateTimeLabel.Name = "dateTimeLabel"
        Me.dateTimeLabel.Size = New System.Drawing.Size(60, 17)
        Me.dateTimeLabel.Text = "Date Time"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No. Faktur"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgressBar, Me.dateTimeLabel})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 469)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(857, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ProgressBar
        '
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(100, 16)
        Me.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'FilterButton
        '
        Me.FilterButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FilterButton.Location = New System.Drawing.Point(746, 81)
        Me.FilterButton.Name = "FilterButton"
        Me.FilterButton.Size = New System.Drawing.Size(99, 42)
        Me.FilterButton.TabIndex = 10
        Me.FilterButton.Text = "Tampilkan"
        Me.FilterButton.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Konsumen"
        '
        'CucianFilterBox
        '
        Me.CucianFilterBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CucianFilterBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CucianFilterBox.Controls.Add(Me.CheckBoxKeluar)
        Me.CucianFilterBox.Controls.Add(Me.CheckBoxMasuk)
        Me.CucianFilterBox.Controls.Add(Me.DateKAkhir)
        Me.CucianFilterBox.Controls.Add(Me.Label6)
        Me.CucianFilterBox.Controls.Add(Me.DateKAwal)
        Me.CucianFilterBox.Controls.Add(Me.DateMAkhir)
        Me.CucianFilterBox.Controls.Add(Me.Label4)
        Me.CucianFilterBox.Controls.Add(Me.DateMAwal)
        Me.CucianFilterBox.Controls.Add(Me.fakturTbx)
        Me.CucianFilterBox.Controls.Add(Me.konsumenTbx)
        Me.CucianFilterBox.Controls.Add(Me.Label2)
        Me.CucianFilterBox.Controls.Add(Me.Label1)
        Me.CucianFilterBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CucianFilterBox.Location = New System.Drawing.Point(12, 6)
        Me.CucianFilterBox.Name = "CucianFilterBox"
        Me.CucianFilterBox.Size = New System.Drawing.Size(728, 117)
        Me.CucianFilterBox.TabIndex = 7
        Me.CucianFilterBox.TabStop = False
        Me.CucianFilterBox.Text = "Filter Data"
        '
        'FAmbilCucian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(857, 491)
        Me.Controls.Add(Me.RefreshButton)
        Me.Controls.Add(Me.dgvCucian)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.FilterButton)
        Me.Controls.Add(Me.CucianFilterBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FAmbilCucian"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ambil Cucian"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvCucian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.CucianFilterBox.ResumeLayout(False)
        Me.CucianFilterBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CheckBoxKeluar As CheckBox
    Friend WithEvents RefreshButton As Button
    Friend WithEvents CheckBoxMasuk As CheckBox
    Friend WithEvents DateKAkhir As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents DateKAwal As DateTimePicker
    Friend WithEvents DateMAkhir As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents DateMAwal As DateTimePicker
    Friend WithEvents fakturTbx As TextBox
    Friend WithEvents konsumenTbx As TextBox
    Friend WithEvents Timer As Timer
    Friend WithEvents dgvCucian As DataGridView
    Friend WithEvents dateTimeLabel As ToolStripStatusLabel
    Friend WithEvents Label2 As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ProgressBar As ToolStripProgressBar
    Friend WithEvents FilterButton As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents CucianFilterBox As GroupBox
End Class
