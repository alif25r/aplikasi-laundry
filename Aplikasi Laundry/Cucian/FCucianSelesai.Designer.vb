﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCucianSelesai
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Id_pelangganLabel As System.Windows.Forms.Label
        Dim Id_jenis_cucianLabel As System.Windows.Forms.Label
        Dim JumlahLabel As System.Windows.Forms.Label
        Dim HargaLabel As System.Windows.Forms.Label
        Dim Total_hargaLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCucianSelesai))
        Me.SelesaiCucianButton = New System.Windows.Forms.Button()
        Me.PelangganTextBox = New System.Windows.Forms.TextBox()
        Me.JumlahTextBox = New System.Windows.Forms.TextBox()
        Me.HargaTextBox = New System.Windows.Forms.TextBox()
        Me.TotalTextBox = New System.Windows.Forms.TextBox()
        Me.JenisCucianTextBox = New System.Windows.Forms.TextBox()
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Id_pelangganLabel = New System.Windows.Forms.Label()
        Id_jenis_cucianLabel = New System.Windows.Forms.Label()
        JumlahLabel = New System.Windows.Forms.Label()
        HargaLabel = New System.Windows.Forms.Label()
        Total_hargaLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'SelesaiCucianButton
        '
        Me.SelesaiCucianButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SelesaiCucianButton.Location = New System.Drawing.Point(9, 172)
        Me.SelesaiCucianButton.Name = "SelesaiCucianButton"
        Me.SelesaiCucianButton.Size = New System.Drawing.Size(280, 37)
        Me.SelesaiCucianButton.TabIndex = 42
        Me.SelesaiCucianButton.Text = "SELESAI"
        Me.SelesaiCucianButton.UseVisualStyleBackColor = True
        '
        'Id_pelangganLabel
        '
        Id_pelangganLabel.AutoSize = True
        Id_pelangganLabel.Location = New System.Drawing.Point(7, 42)
        Id_pelangganLabel.Name = "Id_pelangganLabel"
        Id_pelangganLabel.Size = New System.Drawing.Size(58, 13)
        Id_pelangganLabel.TabIndex = 31
        Id_pelangganLabel.Text = "Pelanggan"
        '
        'PelangganTextBox
        '
        Me.PelangganTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.PelangganTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.PelangganTextBox.Location = New System.Drawing.Point(90, 39)
        Me.PelangganTextBox.Name = "PelangganTextBox"
        Me.PelangganTextBox.ReadOnly = True
        Me.PelangganTextBox.Size = New System.Drawing.Size(200, 20)
        Me.PelangganTextBox.TabIndex = 32
        '
        'Id_jenis_cucianLabel
        '
        Id_jenis_cucianLabel.AutoSize = True
        Id_jenis_cucianLabel.Location = New System.Drawing.Point(6, 69)
        Id_jenis_cucianLabel.Name = "Id_jenis_cucianLabel"
        Id_jenis_cucianLabel.Size = New System.Drawing.Size(67, 13)
        Id_jenis_cucianLabel.TabIndex = 33
        Id_jenis_cucianLabel.Text = "Jenis Cucian"
        '
        'JumlahLabel
        '
        JumlahLabel.AutoSize = True
        JumlahLabel.Location = New System.Drawing.Point(6, 95)
        JumlahLabel.Name = "JumlahLabel"
        JumlahLabel.Size = New System.Drawing.Size(40, 13)
        JumlahLabel.TabIndex = 34
        JumlahLabel.Text = "Jumlah"
        '
        'JumlahTextBox
        '
        Me.JumlahTextBox.Location = New System.Drawing.Point(89, 92)
        Me.JumlahTextBox.Name = "JumlahTextBox"
        Me.JumlahTextBox.ReadOnly = True
        Me.JumlahTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JumlahTextBox.TabIndex = 35
        '
        'HargaLabel
        '
        HargaLabel.AutoSize = True
        HargaLabel.Location = New System.Drawing.Point(6, 121)
        HargaLabel.Name = "HargaLabel"
        HargaLabel.Size = New System.Drawing.Size(36, 13)
        HargaLabel.TabIndex = 38
        HargaLabel.Text = "Harga"
        '
        'HargaTextBox
        '
        Me.HargaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HargaTextBox.Location = New System.Drawing.Point(89, 118)
        Me.HargaTextBox.Name = "HargaTextBox"
        Me.HargaTextBox.ReadOnly = True
        Me.HargaTextBox.Size = New System.Drawing.Size(200, 20)
        Me.HargaTextBox.TabIndex = 39
        Me.HargaTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Total_hargaLabel
        '
        Total_hargaLabel.AutoSize = True
        Total_hargaLabel.Location = New System.Drawing.Point(6, 149)
        Total_hargaLabel.Name = "Total_hargaLabel"
        Total_hargaLabel.Size = New System.Drawing.Size(31, 13)
        Total_hargaLabel.TabIndex = 40
        Total_hargaLabel.Text = "Total"
        '
        'TotalTextBox
        '
        Me.TotalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalTextBox.Location = New System.Drawing.Point(89, 144)
        Me.TotalTextBox.Name = "TotalTextBox"
        Me.TotalTextBox.ReadOnly = True
        Me.TotalTextBox.Size = New System.Drawing.Size(200, 22)
        Me.TotalTextBox.TabIndex = 41
        Me.TotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'JenisCucianTextBox
        '
        Me.JenisCucianTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.JenisCucianTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.JenisCucianTextBox.Location = New System.Drawing.Point(90, 66)
        Me.JenisCucianTextBox.Name = "JenisCucianTextBox"
        Me.JenisCucianTextBox.ReadOnly = True
        Me.JenisCucianTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JenisCucianTextBox.TabIndex = 43
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(7, 16)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(18, 13)
        Label1.TabIndex = 44
        Label1.Text = "ID"
        '
        'IDTextBox
        '
        Me.IDTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.IDTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.IDTextBox.Location = New System.Drawing.Point(90, 13)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.ReadOnly = True
        Me.IDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.IDTextBox.TabIndex = 45
        '
        'FCucianSelesai
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(301, 219)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.IDTextBox)
        Me.Controls.Add(Me.JenisCucianTextBox)
        Me.Controls.Add(Me.SelesaiCucianButton)
        Me.Controls.Add(Id_pelangganLabel)
        Me.Controls.Add(Me.PelangganTextBox)
        Me.Controls.Add(Id_jenis_cucianLabel)
        Me.Controls.Add(JumlahLabel)
        Me.Controls.Add(Me.JumlahTextBox)
        Me.Controls.Add(HargaLabel)
        Me.Controls.Add(Me.HargaTextBox)
        Me.Controls.Add(Total_hargaLabel)
        Me.Controls.Add(Me.TotalTextBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCucianSelesai"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cucian Selesai"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SelesaiCucianButton As Button
    Friend WithEvents PelangganTextBox As TextBox
    Friend WithEvents JumlahTextBox As TextBox
    Friend WithEvents HargaTextBox As TextBox
    Friend WithEvents TotalTextBox As TextBox
    Friend WithEvents JenisCucianTextBox As TextBox
    Friend WithEvents IDTextBox As TextBox
End Class
