﻿Imports System.Data.OleDb
Module Koneksi

    Public str As String
    Public conn As OleDbConnection
    Public cmd As OleDbCommand
    Public dr As OleDbDataReader

    Sub KoneksiDB(Optional ByVal close As Boolean = False)
        If close Then
            conn.Close()
        Else
            str = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + Application.StartupPath + "\..\..\Data Source\Database_Laundry.mdb'"
            conn = New OleDbConnection(str)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            Else
                MsgBox(“Koneksi gagal”)
            End If
        End If
    End Sub

End Module
